#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# global cache
# ------------
"""
This program calculates the maximum Collatz cycle of a given inclusive
interval.

:Input:
    Input will consist of an interval of integers i and j. All
    integers will be less than 1,000,000 and greater than 0.

:Execution:
    Processes all integers in interval and determines the maximum cycle
    length over all integers between i and j.

:Output:
    Outputs the input interval with another column representing the
    maximum cycle
"""

CACHE_SIZE = 1000000 # Number of buckets cache can have from 1-999999 inclusive
# Eager dictionary cache
cache = {1: 1, 3: 8, 5000: 29, 10000: 30, 100000: 129, 837799: 525, 999999: 259}

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # Precondition checks
    assert isinstance(i, int) and isinstance(j, int)
    assert 0 < i < 1000000
    assert 0 < j < 1000000

    if j < i:
        i, j = j, i

    max_cycle = 1

    for idx in range(i, j + 1):
        if idx in cache:
            if cache[idx] > max_cycle:
                max_cycle = cache[idx]
        else:
            cycle = get_cycle(idx)
            cache[idx] = cycle

            if cycle > max_cycle:
                max_cycle = cycle
    # max_cycle must be greater than 0 otherwise return result
    assert max_cycle > 0
    return max_cycle

# ------------
# get_cycle
# ------------

def get_cycle(num):
    """
    Finds Collatz cycle length of an int
    :param int:
        Passed an integeter and a dictionary object of type Cache for
        memorization
    :rtype:
        int
    :return:
        Returns an integer representing the Collatz Cycle length
    """
    assert num > 0

    count = 0

    if num == 1:
        return 1

    while num > 1:

        if is_odd(num):
            num = num * 3 + 1
            count += 1

        num //= 2
        count += 1

        if num < CACHE_SIZE and num in cache:
            """
            num is not in cache if greater than cache size (input
            interval range). Otherwise, check cache for stored cycle.
            """
            count += cache[num]
            num = 1
    return count

# ------------
# is_odd
# ------------

def is_odd(num):
    """
    Function is_odd() determines if an integer is even or odd
    :param int:
    :rtype:
        bool
    :return:
        Returns True if an integer is odd and False otherwise
    """
    if num % 2 != 0:
        return True

    return False

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
