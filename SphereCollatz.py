#!/usr/bin/env python3

# ------------------------------
# projects/collatz/RunCollatz.py
# Copyright (C)
# Glenn P. Downing
# ------------------------------

# -------
# imports
# -------

import sys
#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------

CACHE_SIZE = 1000000 # Number of buckets cache can have from 1-999999 inclusive
cache = {1: 1, 3: 8, 5000: 29, 10000: 30, 100000: 129, 837799: 525, 999999: 259}

def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # Precondition checks
    assert type(i) is int and type(j) is int
    assert 0 < i < 1000000
    assert 0 < j < 1000000

    if j < i:
        tmp = j
        j = i
        i = tmp
        del tmp

    max = 1

    for idx in range(i, j + 1):
        if idx in cache:
            if cache[idx] > max:
                max = cache[idx]
        else:
            cycle = get_cycle(idx, cache)
            cache[idx] = cycle

            
            if cycle > max:
                max = cycle
    assert max > 0
    return max

def get_cycle(num, cache):
    """
    Finds Collatz cycle length of an int
    :param int, object:
        Passed an integeter and a dictionary object of type Cache for
        memorization
    :rtype:
        int
    :return:
        Returns an integer representing the Collatz Cycle length
    """
    count = 0

    if num == 1:
        return 1

    while num > 1:
        
        if is_odd(num):
            num = num * 3 + 1
            count += 1

        num //= 2
        count += 1

        # num does not need to be in cache if it is greater than the 
        # maximum interval range inclusive. Dont check if in cache
        if num < CACHE_SIZE and num in cache:
            count += cache[num]
            return count
    
    count += 1
    return count

def is_odd(num):
	if num % 2 != 0:
		return True
	else:
		return False
# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        if s != '\n':
            i, j = collatz_read(s)
            v = collatz_eval(i, j)
            collatz_print(w, i, j, v)

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
$ cat RunCollatz.in
1 10
100 200
201 210
900 1000

$ python RunCollatz.py < RunCollatz.in > RunCollatz.out

$ cat RunCollatz.out
1 10 1
100 200 1
201 210 1
900 1000 1

$ python -m pydoc -w Collatz"
# That creates the file Collatz.html
"""
